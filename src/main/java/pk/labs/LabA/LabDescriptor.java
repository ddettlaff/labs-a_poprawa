package pk.labs.LabA;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "pk.labs.LabA.Imp.DisplayCompo";
    public static String controlPanelImplClassName = "pk.labs.LabA.Imp.ControlPanelCompo";

    public static String mainComponentSpecClassName = "pk.labs.LabA.Contracts.Main";
    public static String mainComponentImplClassName = "pk.labs.LabA.Imp.MainCompo";
    // endregion

    // region P2
    public static String mainComponentBeanName = "gKomponentImp";
    public static String mainFrameBeanName = "gOkno";
    // endregion

    // region P3
    public static String sillyFrameBeanName = "silly-frame";
    // endregion
}
